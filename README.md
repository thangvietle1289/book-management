Java Spring Exercise!
================
1. Exercise 1: Book
    - Models
    - User
    - Book
    - Pages
    - Login
    - Books List
    - Book Details
    - Book Creation
    - Book Edition
    - Database
    - Documentation
2. Exercise 2: TBD


____

## Exercise 1: Book
The goal of this exercise is to write a small web application using Spring Boot. The application allows to handle books in a library. It should contain five pages, namely Login, Books List, Book Details, Book Creation and Book Edition.

### Models

This sections describes the models used in the application. Feel free to modify them if you believe there is a better definition

#### User

|  Property  |  Required    |    
|------------|------------- |
| id         |      ✓      |
| email      |      ✓      |
| password   |      ✓      |
| firstName  |      ✗      |
| lastName   |      ✗      |

____

#### Book

|  Property   |	    Required    |
|------------ |-----------------|
| id          |         ✓      |
| title       |         ✓      |
| author      |         ✓      |
| description |         ✗      |
| createdAt   |         ✓      |
| updatedAt   |         ✗      |


### Pages
This sections describes the different pages and actions available in the application.

#### Login
This is the entry page of the application. It provides a form for a user to authenticate. Upon successful authentication, the user is redirected to the Books List page. When a logged in user access the application, the entry page is the Books List. Please create at least one valid User for testing.

#### Books List
This page displays a list of the available Books. The list is initially empty and there is a button to redirect to the Book Creation page. Each list item displays the title and the author of the Book as well as two action buttons; the first one is used to update the Book and the second one to delete the Book.

#### Book Details
This page displays all details of a Book.

#### Book Creation
This page displays a form to create a new Book.

#### Book Edition
This page displays a form allowing a user to modify the title, author and description of a Book.

### Database
The models should be stored in a SQL database. You can use the technology of your choice, with a preference for embedded database like H2 which will simplify testing setup.

### Documentation
The solution should include the necessary documentation to run the application and explain important decisions.

## Exercise 2: TBD

____

Installation project
================

##  Server side
### Create database in MYSQL
- Create databases
	+ bookmanagement for server running
	+ bookmanagement_test for unit testing

### Start server
- Run cmd
- Go to current project folder @PATH_NAME/server
- Run server(default port 8080): 
```sh
> mvn spring-boot:run
```
   		
##  Client side
### Install Nodejs 
- Download from https://nodejs.org/
- Install from downloaded file.

### Start client server
- Run cmd
- Go to current project folder @PATH_NAME/client
- npm install
- Run server(default port 3000): 
```sh
> node server.js
```
- Open browser and enter http://localhost:3000
	- Default account: 
		- email: admin@gmail.com
		- password: admin

____

## List APIs

### User
|  		       | Add new user    		         |
|--------------|---------------------------------|
| URL          | http://localhost:8080/api/user  |
| Method       |             POST                |
| Content-type |         application/json        |
| Data         | { "email": "your email",        |
|			   |   "password": "your password",  |
|			   |   "firstname": "your firstname",|
|			   |   "lastname": "your lastname"}  |


|  		       | Update User    		             |
|--------------|-------------------------------------|
| URL          | http://localhost:8080/api/user/{id} |
| Method       |             POST                    |
| Content-type |         application/json            |
| Data         | { "email": "your email",            |
|			   |   "password": "your password",      |
|			   |   "firstname": "your firstname",    |
|			   |   "lastname": "your lastname"}      |

|  		       | Get all user    		         |
|--------------|-------------------------------------|
| URL          | http://localhost:8080/api/user/     |
| Method       |                GET                  |

|  		       | Get user details    		         |
|--------------|-------------------------------------|
| URL          | http://localhost:8080/api/user/{id} |
| Method       |                GET                  |

|  		       | Delete user    		             |
|--------------|-------------------------------------|
| URL          | http://localhost:8080/api/user/{id} |
| Method       |                DELETE               |

|  		       | Login user          		          |
|--------------|--------------------------------------|
| URL          | http://localhost:8080/api/user/login |
| Method       |                 POST                 |
| Data         | { "email": "your email",             |
|			   |   "password": "your password" }      |

### Book
|  		       | Add new book    		                   |
|--------------|-------------------------------------------|
| URL          | http://localhost:8080/api/{user_id}/book  |
| Method       |                   POST                    |
| Content-type |             application/json              |
| Data         | { "title": "title",                       |
|			   |   "author": "author",                     |
|			   |   "description": "description",           |
|			   |   "createAt": "yyyy-mm-dd"}               |

|  		       | Update book    		                        |
|--------------|------------------------------------------------|
| URL          | http://localhost:8080/api/{user_id}/book/{id}  |
| Method       |                       POST                     |
| Content-type |                 application/json               |
| Data         | { "title": "title",                            |
|			   |   "author": "author",                          |
|			   |   "description": "description",                |
|			   |   "updateAt": "yyyy-mm-dd"}                    |

|  		       | Get book details 		                        |
|--------------|------------------------------------------------|
| URL          | http://localhost:8080/api/{user_id}/book/{id}  |
| Method       |                   GET                          |


|  		       | Delete book    		                        |
|--------------|------------------------------------------------|
| URL          | http://localhost:8080/api/{user_id}/book/{id}  |
| Method       |                     DELETE                     |

|  		       | Find book    		                                                  |
|--------------|----------------------------------------------------------------------|
| URL          | http://localhost:8080/api/{user_id}/book/find?keyword[=YOUR_KEYWORD] |
| Method       |                                  GET                                 |

____

[spring boot rest api]: <http://www.petrikainulainen.net/programming/spring-framework/creating-a-rest-api-with-spring-boot-and-mongodb/>
[spring boot rest api ]: <http://spring.io/guides/tutorials/bookmarks/>
[access data jpa]: <https://spring.io/guides/gs/accessing-data-jpa/>
[rest cors]: <https://spring.io/guides/gs/rest-service-cors/>
[style login form]: <http://bootsnipp.com/snippets/OePK8>
[express render html file]: <https://codeforgeek.com/2015/01/render-html-file-expressjs/>
