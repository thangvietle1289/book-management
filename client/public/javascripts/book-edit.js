var user_id = Cookies.get('user_id');

$(document).ready(function(){
	$('#inputUpdateAt').datepicker({
  	format: "yyyy-mm-dd"
  });

	var arr_path = window.location.pathname.split("/");
	var id = arr_path[arr_path.length-1];
	var createAt;

	console.log("id : ", id);
	
	$.ajax({
		type: 'GET',
		url: 'http://localhost:8080/api/' + user_id + '/book/' + id,
		success: function(data, textStatus){
			console.log(data);
			if(data != null) {
				$("#inputTitle").val(data.title);
				$("#inputAuthor").val(data.author);
				$("#inputDescription").val(data.description);
				$("#inputUpdateAt").val(data.updateAt);
				createAt = data.createAt;
			}
		},
		error: function(xhr, textStatus, errorThrown){
			console.log('xhr: ', xhr, ' - status: ', status, "- errorThrown: ", errorThrown);
			$(".notify").html('<div class="alert alert-danger"><strong>Error!</strong> This book is not existing.</div>');
		}
	});


	$("#update-book").click(function(){
	  var title = $('#inputTitle').val();
	  var author = $('#inputAuthor').val();
	  var description = $('#inputDescription').val();
	  var updateAt = $('#inputUpdateAt').val();

	  console.log('title: ', title, " - author: ", author, " - description: ", description, " - updateAt : ", updateAt);

	  if(title == "") {
	  	$(".notify").html('<div class="alert alert-danger">Please enter title of book.</div>');
	  	return;
	  }
	  if(author == "") {
	  	$(".notify").html('<div class="alert alert-danger">Please enter author of book.</div>');
	  	return;
	  }

	  if(updateAt == "") {
	  	$(".notify").html('<div class="alert alert-danger">Please enter update date of book.</div>');
	  	return;
	  }

	  $.ajax({
			type: 'POST',
			contentType:'application/json',
			url: 'http://localhost:8080/api/' + user_id + '/book/' + id,
			data:JSON.stringify({"id": id, "title":title, "author":author, "description":description, "createAt":createAt, "updateAt":updateAt}),
			success: function(data, textStatus){
				console.log(data);
				if(data != null) {
					$(".notify").html('<div class="alert alert-success">Update book successful!</div>');
					window.location.href = "/book-list";
				}
			},
			error: function(xhr, textStatus, errorThrown){
				console.log('xhr: ', xhr, ' - status: ', status, "- errorThrown: ", errorThrown);
				$(".notify").html('<div class="alert alert-danger"><strong>Error!</strong> Update book.</div>');
			}
		});
	});
});