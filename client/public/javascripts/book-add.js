var user_id = Cookies.get('user_id');

function addBook(isClose) {
	var title = $('#inputTitle').val();
  var author = $('#inputAuthor').val();
  var description = $('#inputDescription').val();
  var createAt = $('#inputCreateAt').val();

  console.log('title: ', title, " - author: ", author, " - description: ", description, " - createAt : ", createAt);

  if(title == "") {
  	$(".notify").html('<div class="alert alert-danger">Please enter title of book.</div>');
  	return;
  }
  if(author == "") {
  	$(".notify").html('<div class="alert alert-danger">Please enter author of book.</div>');
  	return;
  }

  if(createAt == "") {
  	$(".notify").html('<div class="alert alert-danger">Please enter date create of book.</div>');
  	return;
  }

  $.ajax({
		type: 'POST',
		dataType: 'json',
		contentType:'application/json',
		url: 'http://localhost:8080/api/' + user_id + '/book',
		data:JSON.stringify({"title":title, "author":author,"description":description,"createAt":createAt}),
		success: function(data, textStatus){
			console.log(data);
			if(data != null) {
				$(".notify").html('<div class="alert alert-success">Add new book successful!</div>');
				$('#form-add-book').trigger("reset");
				if(isClose) {
					window.location.href = "/book-list";
				}
			}
		},
		error: function(xhr, textStatus, errorThrown){
			console.log('xhr: ', xhr, ' - status: ', status, "- errorThrown: ", errorThrown);
			$(".notify").html('<div class="alert alert-danger"><strong>Error!</strong> Please enter correct your book info.</div>');
		}
	});
}

$(document).ready(function(){
	
	$('#inputCreateAt').datepicker({
  	format: "yyyy-mm-dd"
  });

  $("#cancel").click(function(){
  	$('#form-add-book').trigger("reset");
  });

});