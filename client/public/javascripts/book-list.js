var user_id = Cookies.get('user_id');

function handleDeleteBook(id) {
  console.log("delete book id :", id);
  $.ajax({
		type: 'DELETE',
		url: 'http://localhost:8080/api/' + user_id + '/book/' + id,
		success: function(data, textStatus){
			console.log(data);
			if(data != null) {
				$(".notify").html('<div class="alert alert-success">Delete book successful!</div>');
				loadListBook();
			}
		},
		error: function(xhr, textStatus, errorThrown){
			console.log('xhr: ', xhr, ' - status: ', status, "- errorThrown: ", errorThrown);
			$(".notify").html('<div class="alert alert-danger"><strong>Error!</strong> Cannot delete this book.</div>');
		}
	});
}

function loadListBook() {
	console.log("USER ID : " + user_id);

	$.ajax({
		type: 'GET',
		url: 'http://localhost:8080/api/' + user_id + '/book',
		success: function(data, textStatus){
			console.log(data);
			if(data != null) {
				$("#list-book").html("");
				$("#list-book").append('<ul class="list-group"></ul>');
				$.each(data, function(idx, obj) {
					$(".list-group").append('<li class="list-group-item clearfix">\
						<a href="/book-details/' + obj.id + '" class="">\
							<h4 class="list-group-item-heading">' + obj.title + '</h4>\
						</a>' + obj.author + 
						'<span class="pull-right button-group">\
							<a href="/book-edit/' + obj.id + '" class="btn btn-primary edit-book-action">\
								<span class="glyphicon glyphicon-edit"></span>\
							</a>\
							<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal' + obj.id + '">\
								<span class="glyphicon glyphicon-remove"></span>\
							</button>\
						</span>\
						</li>');

					$(".list-group").append('<div class="modal fade" id="deleteModal' + obj.id + '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">\
						  <div class="modal-dialog" role="document">\
						    <div class="modal-content">\
						      <div class="modal-header">\
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
						        <h4 class="modal-title" id="myModalLabel">Delete modal</h4>\
						      </div>\
						      <div class="modal-body">\
						        <h4>Do you want to delete this book?</h4>\
						      </div>\
						      <div class="modal-footer">\
						        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\
						        <button type="button" class="btn btn-danger" data-dismiss="modal" onClick="handleDeleteBook(' + obj.id + ')">Delete</button>\
						      </div>\
						    </div>\
						  </div>\
						</div>');


				});
			}
		},
		error: function(xhr, textStatus, errorThrown){
			console.log('xhr: ', xhr, ' - status: ', status, "- errorThrown: ", errorThrown);
			$(".notify").html('<div class="alert alert-info"><strong>Please add new book!</div>');
		}
	});
}

$(document).ready(function(){
	$('.add_book').tooltip();

	$(".add_book").click(function(){
		window.location.href = "/book-add";
	});

	console.log("user id : ", user_id);
	if(user_id == undefined) {
		$(".notify").html('<div class="alert alert-danger">Please click <a href='/'>here</a> to login!</div>');
	} else {
		loadListBook();		
	}

});