var user_id = Cookies.get('user_id');

$(document).ready(function(){
	$('.logout').tooltip();
	$('.logout').click(function(){
		Cookies.remove('user_id');
		window.location.href = "/";
	});
});