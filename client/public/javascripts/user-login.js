$("#login").click(function(){
  var email = $('#email').val();
  var password = $('#password').val();
  console.log('email: ', email, " - password: ", password);

  if(email == "") {
  	$(".notify").html('<div class="alert alert-danger">Please enter your email.</div>');
  	return;
  }
  if(password == "") {
  	$(".notify").html('<div class="alert alert-danger">Please enter your password.</div>');
  	return;
  }

  $.ajax({
		type: 'POST',
		dataType: 'json',
		contentType:'application/json',
		url: "http://localhost:8080/api/user/login",
		data:JSON.stringify({"email":email, "password":password}),
		success: function(data, textStatus){
			console.log(data);
			if(data.email != null) {
				$(".notify").html("");
				Cookies.set('user_id', data.id, { expires: 1 });
				window.location.href = "/book-list";
			}
		},
		error: function(xhr, textStatus, errorThrown){
			console.log('xhr: ', xhr, ' - status: ', status, "- errorThrown: ", errorThrown);
			if(xhr.responseJSON == undefined) {
				$(".notify").html('<div class="alert alert-danger"><strong>Error!</strong> Please start server site.</div>');	
			} else {
				$(".notify").html('<div class="alert alert-danger"><strong>Error!</strong> Please enter correct your email and password.</div>');	
			}
			
		}
	});
});

