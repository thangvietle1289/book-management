var user_id = Cookies.get('user_id');

$(document).ready(function(){
	$('.logout').tooltip();
	
	var arr_path = window.location.pathname.split("/");
	var id = arr_path[arr_path.length-1];

	if(id > 0) {
		console.log("id : ", id);
	}

	$.ajax({
		type: 'GET',
		url: 'http://localhost:8080/api/' + user_id + '/book/' + id,
		success: function(data, textStatus){
			console.log(data);
			if(data != null) {
				$(".book-title").html(data.title);
				$(".book-author").html(data.author);
				$(".book-description").html(data.description);
				$(".book-date").html('<strong>Originally published:</strong> ' + data.createAt);
			}
		},
		error: function(xhr, textStatus, errorThrown){
			console.log('xhr: ', xhr, ' - status: ', status, "- errorThrown: ", errorThrown);
			$(".notify").html('<div class="alert alert-danger"><strong>Error!</strong> Cannot get book details.</div>');
		}
	});
});