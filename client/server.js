var path    = require("path");
var express = require('express');

var app = express();

var oneDay = 86400000;

app.use(express.static(__dirname + '/public', { maxAge: oneDay }));

app.get('/', function (req, res) {
  res.sendFile('index.html');
});

app.get('/signup', function (req, res) {
  res.sendFile(path.join(__dirname+'/public/signup.html'));
});

app.get('/book-list', function (req, res) {
  res.sendFile(path.join(__dirname+'/public/list_book.html'));
});

app.get('/book-details/:id', function (req, res) {
  res.sendFile(path.join(__dirname+'/public/book_details.html'));
});

app.get('/book-add', function (req, res) {
  res.sendFile(path.join(__dirname+'/public/add_book.html'));
});

app.get('/book-edit/:id', function (req, res) {
  res.sendFile(path.join(__dirname+'/public/edit_book.html'));
});

app.listen(process.env.PORT || 3000);
console.log('Starting server at port 3000 ...');