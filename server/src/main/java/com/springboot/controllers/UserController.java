package com.springboot.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.models.Login;
import com.springboot.models.User;
import com.springboot.services.UserService;

@RestController
@RequestMapping("/api/user")
public class UserController {
	
	private UserService userService;
	
	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}

	@RequestMapping(method = RequestMethod.GET)
	public List<User> getAllUsers() {
		return userService.listAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public User getUser(@PathVariable("id") int id) {
		return userService.get(id);
	}

	@RequestMapping(method = RequestMethod.POST)
	public User addUser(@Valid @RequestBody User user) {
		return userService.save(user);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	public User updateUser(@PathVariable("id") int id, @Valid @RequestBody User user) {
		user.setId(id);
		return userService.update(user);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public User deleteUser(@PathVariable("id") int id) {
		return userService.delete(id);
	}
	
//	@RequestMapping(value = "/login", method = RequestMethod.POST)
//	public User loginUser(@RequestParam String email, @RequestParam String password) {
//		System.out.println("Email: " + email + " - Password: " + password);
//		User user = userService.login(email, password);
//		System.out.println("User : " + user);
//		return user;
////		return userService.login(email, password);
//	}
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public User loginUser(@RequestBody Login login) {
		System.out.println("Account login: " + login.toString());
		User user = userService.login(login.getEmail(), login.getPassword());
		System.out.println("User : " + user);
		return user;
//		return userService.login(email, password);
	}
}
