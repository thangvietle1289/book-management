package com.springboot.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.exceptions.NotFoundException;
import com.springboot.models.Book;
import com.springboot.models.User;
import com.springboot.services.BookService;
import com.springboot.services.UserService;

@RestController
@RequestMapping("/api")
public class BookController {

	private BookService bookService;
	private UserService userService;

	@Autowired
	public BookController(BookService bookService, UserService userService) {
		this.bookService = bookService;
		this.userService = userService;
	}

	@RequestMapping(value = "/{user_id}/book", method = RequestMethod.GET)
	public List<Book> listBook(@PathVariable("user_id") long user_id) {
		User user = userService.get(user_id);
		if (user == null) {
			throw new NotFoundException("The 'user_id' parameter is not existing. Please enter other user id!");
		}
		return bookService.findByUser(user);
	}

	@RequestMapping(value = "/{user_id}/book/{id}", method = RequestMethod.GET)
	public Book getBook(@PathVariable("user_id") long user_id, @PathVariable("id") long id) {
		User user = userService.get(user_id);
		if (user == null) {
			throw new NotFoundException("The 'user_id' parameter is not existing. Please enter other user id!");
		}
		return bookService.get(user, id);
	}

	@RequestMapping(value = "/{user_id}/book", method = RequestMethod.POST)
	public Book addBook(@PathVariable("user_id") long user_id, @Valid @RequestBody Book book) {
		User user = userService.get(user_id);
		if (user == null) {
			throw new NotFoundException("The 'user_id' parameter is not existing. Please enter other user id!");
		}
		book.setUser(user);
		return bookService.save(book);
	}

	@RequestMapping(value = "/{user_id}/book/{id}", method = RequestMethod.POST)
	public Book updateBook(@PathVariable("user_id") long user_id, @PathVariable("id") long id,
			@Valid @RequestBody Book book) {
		
		User user = userService.get(user_id);
		if (user == null) {
			throw new NotFoundException("The 'user_id' parameter is not existing. Please enter other user id!");
		}
		book.setId(id);
		book.setUser(user);
		return bookService.update(book);
	}

	@RequestMapping(value = "/{user_id}/book/{id}", method = RequestMethod.DELETE)
	public Book deleteBook(@PathVariable("user_id") long user_id, @PathVariable("id") long id) {
		User user = userService.get(user_id);
		if (user == null) {
			throw new NotFoundException("The 'user_id' parameter is not existing. Please enter other user id!");
		}
		return bookService.delete(user, id);
	}

	@RequestMapping(value = "/{user_id}/book/find", method = RequestMethod.GET)
	public List<Book> findBook(@PathVariable("user_id") long user_id, @RequestParam("keyword") String keyword) {
		User user = userService.get(user_id);
		if (user == null) {
			throw new NotFoundException("The 'user_id' parameter is not existing. Please enter other user id!");
		}
		return bookService.findByKeyword(user, keyword);
	}
}
