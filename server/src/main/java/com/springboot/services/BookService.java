package com.springboot.services;

import java.util.List;

import com.springboot.models.Book;
import com.springboot.models.User;

public interface BookService {

	Book save(Book book);
    
    Book update(Book book);
    
    Book delete(User user, Long id);
    
    Book get(User user, Long id);
    
    List<Book> findByKeyword(User user, String keyword);
    
    List<Book> findByUser(User user);

    List<Book> listAll();
    
}
