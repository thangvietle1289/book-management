package com.springboot.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.exceptions.AlreadyException;
import com.springboot.exceptions.NotFoundException;
import com.springboot.models.User;
import com.springboot.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository repository;

	@Override
	public User save(User user) {
		// check email is existing
		User foundUser = repository.findByEmail(user.getEmail());
		if (foundUser != null) {
			 throw new AlreadyException("This email is existing!");
		}
		return repository.save(user);
	}

	@Override
	public User update(User user) {
		User foundUser = repository.findOne(user.getId());
		if (foundUser == null) {
			 throw new NotFoundException("This user is not existing!");
		}

		if (foundUser.getEmail() != user.getEmail()) {
			User findUserByEmail = repository.findByEmail(user.getEmail());
			if (findUserByEmail != null) {
				 throw new AlreadyException("This email is existing!");
			}
		}
		foundUser.setEmail(user.getEmail());
		foundUser.setFirstName(user.getFirstName());
		foundUser.setLastName(user.getLastName());
		return repository.save(foundUser);
	}

	@Override
	public User delete(long id) {
		// check id is exist
		User user = repository.findOne(id);
		if (user == null) {
			 throw new NotFoundException("This user is not existing!");
		}
		repository.delete(id);
		return user;
	}

	@Override
	public User get(long id) {
		User user = repository.findOne(id);
		if (user == null) {
			 throw new NotFoundException("This user is not existing!");
		}
		return user;
	}

	@Override
	public List<User> listAll() {
		return (List<User>) repository.findAll();
	}

	@Override
	public User findUserByEmail(String email) {
		User user = repository.findByEmail(email);
		if (user == null) {
			 throw new NotFoundException("This user is not existing!");
		}
		return user;
	}

	@Override
	public User login(String email, String password) {
		User user = repository.findByEmail(email);
		if (user == null) {
			 throw new NotFoundException("This email is not existing!");
		}
		
		user = repository.findByEmailAndPassword(email, password);
		if(user == null) {
			 throw new NotFoundException("Please enter correct your password!");
		}
		return user;
	}

}
