package com.springboot.services;

import java.util.List;

import com.springboot.models.User;

public interface UserService {
	
	User save(User user);

	User update(User user);
	
	User login(String email, String password);

	User delete(long id);
	
	User get(long id);
	
	User findUserByEmail(String email);

	List<User> listAll();
}
