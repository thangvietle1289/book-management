package com.springboot.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.exceptions.AlreadyException;
import com.springboot.exceptions.NotFoundException;
import com.springboot.models.Book;
import com.springboot.models.User;
import com.springboot.repositories.BookRepository;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BookRepository repository;

	@Override
	public Book save(Book book) {
		// Check title book is existing
		Book foundBook = repository.findByUserAndTitle(book.getUser(), book.getTitle());
		if (foundBook != null) {
			 throw new AlreadyException("This book is existing!");
		}
		book.setUpdateAt(book.getCreateAt());
		return repository.save(book);
	}

	@Override
	public Book update(Book book) {
		Book foundBook = repository.findOne(book.getId());
		if (foundBook == null) {
			 throw new NotFoundException("This book is not existing!");
		}

		if (!foundBook.getTitle().equals(book.getTitle())) {
			Book findBookByTitle = repository.findByUserAndTitle(book.getUser(), book.getTitle());
			if (findBookByTitle != null) {
				 throw new AlreadyException("This title book is existing!");
			}
		}
		
		foundBook.setTitle(book.getTitle());
		foundBook.setAuthor(book.getAuthor());
		foundBook.setDescription(book.getDescription());
		foundBook.setUpdateAt(book.getUpdateAt());
		return repository.save(foundBook);
	}

	@Override
	public Book delete(User user, Long id) {
		// check id is exist
		Book book = repository.findByUserAndId(user, id);
		if (book == null) {
			 throw new NotFoundException("This book is not existing!");
		}
		repository.delete(id);
		return book;
	}

	@Override
	public Book get(User user, Long id) {
		Book book = repository.findByUserAndId(user, id);
		if (book == null) {
			 throw new NotFoundException("This book is not existing!");
		}
		return book;
	}

	@Override
	public List<Book> listAll() {
		return (List<Book>) repository.findAll();
	}

	@Override
	public List<Book> findByKeyword(User user, String keyword) {
		if(keyword.equals("")) {
			return (List<Book>) repository.findAll();
		}
		
		List<Book> bookList = repository.findByKeyword(user, keyword);
		if (bookList == null || bookList.size() == 0) {
			 throw new NotFoundException("No result. Please enter other keyword!");
		}
		return bookList;
	}

	@Override
	public List<Book> findByUser(User user) {
		List<Book> bookList = repository.findByUser(user);
		if (bookList == null || bookList.size() == 0) {
			 throw new NotFoundException("No result!");
		}
		return bookList;
	}

}
