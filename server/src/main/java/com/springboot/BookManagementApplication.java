package com.springboot;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.springboot.models.User;
import com.springboot.repositories.UserRepository;

@SpringBootApplication
public class BookManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookManagementApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(UserRepository repository) {
		return (args) -> {
			// save default user
			String email = "admin@gmail.com";
			User user = repository.findByEmail(email);
			if (user == null) {
				repository.save(new User(email, "admin", "Admin", "Lee"));
			}
		};
	}
}
