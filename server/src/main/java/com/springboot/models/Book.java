package com.springboot.models;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "book_id")
	private long id;

	@NotNull
	@Column(name = "title", nullable = false)
	private String title;

	@NotNull
	@Column(name = "author", nullable = false)
	private String author;

	@Column(name = "description")
	private String description;

	@NotNull
	@Column(name = "createAt", nullable = false)
	private Date createAt;

	@Column(name = "updateAt")
	private Date updateAt;

	// relationship 1-1 with User table
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "user", referencedColumnName = "id", insertable = true, updatable = true)
	private User user;

	public Book() {
	}

	public Book(String title, String author, String description, Date createAt, Date updateAt) {
		this.title = title;
		this.author = author;
		this.description = description;
		this.createAt = createAt;
		this.updateAt = updateAt;
	}

	public Book(User user, String title, String author, String description, Date createAt, Date updateAt) {
		this.title = title;
		this.author = author;
		this.description = description;
		this.createAt = createAt;
		this.updateAt = updateAt;
		this.user = user;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return new StringBuffer().append("id = " + id).append("user id = " + user.getId())
				.append("title = " + title).append("author = " + author)
				.append("description = " + description).append("create at = " + createAt)
				.append("update at = " + updateAt).toString();
	}

}
