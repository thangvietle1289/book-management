package com.springboot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.models.Book;
import com.springboot.models.User;

@Transactional
public interface BookRepository extends CrudRepository<Book, Long> {

	public Book findByUserAndTitle(User user, String title);

	public Book findByUserAndAuthor(User user, String author);

	@Query("SELECT b FROM Book b WHERE b.user=:user AND " +
			"(LOWER(b.title) LIKE LOWER(CONCAT('%',:keyword, '%')) OR " +
			"LOWER(b.author) LIKE LOWER(CONCAT('%',:keyword, '%')) OR " +
			"LOWER(b.createAt) LIKE LOWER(CONCAT('%',:keyword, '%')) OR " +
			"LOWER(b.updateAt) LIKE LOWER(CONCAT('%',:keyword, '%')))")
	public List<Book> findByKeyword(@Param("user") User user, @Param("keyword") String keyword);
	
	public List<Book> findByUser(User user);
	
	public Book findByUserAndId(User user, long id);
}
