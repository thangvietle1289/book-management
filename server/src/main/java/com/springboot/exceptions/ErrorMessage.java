package com.springboot.exceptions;

import java.util.Calendar;
import java.util.Map;

public class ErrorMessage {
	private String status;
	private String code;
	private String path;
	private Long timestamp;
	private Map<String, String> errors;

	public ErrorMessage() {
	}

	public ErrorMessage(Map<String, String> errors) {
		this.errors = errors;
	}

	public ErrorMessage(Map<String, String> errors, String status, String code, String path) {
		this.errors = errors;
		this.status = status;
		this.code = code;
		this.path = path;
		this.timestamp = Calendar.getInstance().getTimeInMillis();
	}

	// public ErrorMessage(String error) {
	// this(Collections.singletonList(error));
	// }
	//
	// public ErrorMessage(String... errors) {
	// this(Arrays.asList(errors));
	// }

	public Map<String, String> getErrors() {
		return errors;
	}

	public void setErrors(Map<String, String> errors) {
		this.errors = errors;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

}
