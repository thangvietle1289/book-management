package com.springboot.exceptions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
		List<ObjectError> globalErrors = ex.getBindingResult().getGlobalErrors();
		Map<String, String> errors = new HashMap<>();
		for (FieldError fieldError : fieldErrors) {
			errors.put(fieldError.getField(), fieldError.getDefaultMessage());
		}
		for (ObjectError objectError : globalErrors) {
			errors.put(objectError.getObjectName(), objectError.getDefaultMessage());
		}
		ErrorMessage errorMessage = new ErrorMessage(errors, status.getReasonPhrase(),
				HttpStatus.BAD_REQUEST.toString(), request.getDescription(false).toString().replace("uri=", ""));
		return new ResponseEntity<Object>(errorMessage, headers, status);
	}

	@Override
	protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		Map<String, String> errors = new HashMap<>();
		errors.put("Unsupported content type", ex.getContentType().toString());
		errors.put("Supported content types", MediaType.toString(ex.getSupportedMediaTypes()));
		ErrorMessage errorMessage = new ErrorMessage(errors, status.getReasonPhrase(),
				HttpStatus.BAD_REQUEST.toString(), request.getDescription(false).toString().replace("uri=", ""));

		return new ResponseEntity<Object>(errorMessage, headers, status);
	}

	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		Throwable mostSpecificCause = ex.getMostSpecificCause();
		Map<String, String> errors = new HashMap<>();
		if (mostSpecificCause != null) {
			String exceptionName = mostSpecificCause.getClass().getName();
			String message = mostSpecificCause.getMessage();
			errors.put(exceptionName, message);
		} else {
			errors.put("message", ex.getMessage());
		}
		ErrorMessage errorMessage = new ErrorMessage(errors, status.getReasonPhrase(),
				HttpStatus.BAD_REQUEST.toString(), request.getDescription(false).toString().replace("uri=", ""));
		return new ResponseEntity<Object>(errorMessage, headers, status);
	}
}