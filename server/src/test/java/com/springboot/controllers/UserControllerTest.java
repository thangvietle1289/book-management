package com.springboot.controllers;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import static org.hamcrest.Matchers.hasSize;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.springboot.BookManagementApplication;
import com.springboot.models.User;
import com.springboot.repositories.UserRepository;
import com.springboot.utils.BookUtilTest;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BookManagementApplication.class)
@WebAppConfiguration
public class UserControllerTest {
	private MockMvc mockMvc;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Before
	public void setup() throws Exception {
		userRepository.deleteAll();
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
		userRepository.save(new User("test1@gmail.com", "test1", "test", "1"));
		userRepository.save(new User("test2@gmail.com", "test2", "test", "2"));
	}

	@Test
	public void userFindAll() throws Exception {
		mockMvc.perform(get("/api/user")).andExpect(status().isOk())
				.andExpect(content().contentType(BookUtilTest.contentType))
				.andExpect(jsonPath("$", hasSize(2)));
	}

	@Test
	public void userNotFound() throws Exception {
		mockMvc.perform(get("/api/user/{id}", 0L)).andExpect(status().isNotFound());
	}

	@Test
	public void userAdd() throws Exception {
		mockMvc.perform(post("/api/user").contentType(BookUtilTest.contentType)
				.content(BookUtilTest.convertObjectToJsonBytes(new User("test3@gmail.com", "test3", "TEST", "3"))))
				.andExpect(status().isOk()).andExpect(jsonPath("$.email", is("test3@gmail.com")));
	}

	@Test
	public void userEdit() throws Exception {
		User user = userRepository.save(new User("user-edit@gmail.com", "123456", "Edit", "User"));
		System.out.println("User : " + user);

		mockMvc.perform(post("/api/user/{id}", user.getId()).contentType(BookUtilTest.contentType)
				.content(BookUtilTest
						.convertObjectToJsonBytes(new User("user-edit111@gmail.com", "123456", "Edit", "User1"))))
				.andExpect(status().isOk()).andExpect(jsonPath("$.email", is("user-edit111@gmail.com")))
				.andExpect(jsonPath("$.lastName", is("User1")));
	}

	@Test
	public void userDelete() throws Exception {
		User user = userRepository.save(new User("user-delete@gmail.com", "123456", "Delete", "User"));
		System.out.println("User : " + user);

		mockMvc.perform(delete("/api/user/{id}", user.getId()).contentType(BookUtilTest.contentType))
				.andExpect(status().isOk()).andExpect(jsonPath("$.email", is("user-delete@gmail.com")))
				.andExpect(jsonPath("$.lastName", is("User")));
	}

}
