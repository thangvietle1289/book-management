package com.springboot.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.sql.Date;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.springboot.BookManagementApplication;
import com.springboot.models.Book;
import com.springboot.models.User;
import com.springboot.repositories.BookRepository;
import com.springboot.repositories.UserRepository;
import com.springboot.utils.BookUtilTest;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BookManagementApplication.class)
@WebAppConfiguration
public class BookControllerTest {
	private MockMvc mockMvc;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BookRepository bookRepository;

	@Autowired
	private WebApplicationContext webApplicationContext;
	
	private User user;

	public Date getDate() {
		Calendar cal = Calendar.getInstance();
		cal.set(2003, 10, 10);
		return new Date(cal.getTimeInMillis());
	}

	@Before
	public void setup() throws Exception {
		bookRepository.deleteAll();
		userRepository.deleteAll();
		
		this.mockMvc = webAppContextSetup(webApplicationContext).build();

		// create new user
		user = userRepository.save(new User("admin@gmail.com", "admin", "Admin", "Lee"));
		
		bookRepository.save(new Book(user, "Java Programming Language", "Ken Arnold, James Gosling, David Holmes",
				"Direct from the creators of the Java, The Java Programming Language is an indispensible resource for novice and advanced programmers alike",
				getDate(), getDate()));
		bookRepository.save(new Book(user, "Thinking In Java", "Bruce Eckel",
				"Eckel introduces all the basics of objects as Java uses them, then walks carefully through the fundamental concepts underlying all Java programming",
				getDate(), getDate()));
		
	}

	@Test
	public void bookFindAll() throws Exception {
		mockMvc.perform(get("/api/{userId}/book", user.getId())).andExpect(status().isOk())
				.andExpect(content().contentType(BookUtilTest.contentType)).andExpect(jsonPath("$", hasSize(2)));
	}

	@Test
	public void bookNotFound() throws Exception {
		mockMvc.perform(get("/api/{userId}/book/{id}", user.getId(), 0L)).andExpect(status().isNotFound());
	}

	@Test
	public void bookAdd() throws Exception {
		mockMvc.perform(post("/api/{userId}/book", user.getId()).contentType(BookUtilTest.contentType)
				.content(BookUtilTest.convertObjectToJsonBytes(new Book("Effective Java", "Joshua Bloch",
						"Joshua brings together seventy-eight indispensable programmer’s rules of thumb: working, best-practice solutions for the programming challenges you encounter every day.",
						getDate(), getDate()))))
				.andExpect(status().isOk()).andExpect(jsonPath("$.title", is("Effective Java")))
				.andExpect(status().isOk()).andExpect(jsonPath("$.author", is("Joshua Bloch")));
	}

	@Test
	public void bookEdit() throws Exception {
		Book book = bookRepository.save(new Book("Head First Java, 2nd Edition", "Kathy Sierra, Bert Bates",
				"Those looking to learn coding in JAVA, should refer to Head First Java, 2nd Edition.", getDate(),
				getDate()));

		mockMvc.perform(post("/api/{userId}/book/{id}", user.getId(), book.getId()).contentType(BookUtilTest.contentType)
				.content(BookUtilTest.convertObjectToJsonBytes(
						new Book("Head First Java, 2nd Edition 111", "Kathy Sierra, Bert Bates 111",
								"Those looking to learn coding in JAVA, should refer to Head First Java, 2nd Edition.",
								getDate(), getDate()))))
				.andExpect(status().isOk()).andExpect(jsonPath("$.title", is("Head First Java, 2nd Edition 111")))
				.andExpect(jsonPath("$.author", is("Kathy Sierra, Bert Bates 111")));
	}

	@Test
	public void bookDelete() throws Exception {
		Book book = bookRepository
				.save(new Book(user, "Thinking in Java111", "Bruce Eckel", "Thinking in Java111", getDate(), getDate()));

		mockMvc.perform(delete("/api/{userId}/book/{id}", user.getId(), book.getId()).contentType(BookUtilTest.contentType))
				.andExpect(status().isOk()).andExpect(jsonPath("$.title", is("Thinking in Java111")))
				.andExpect(jsonPath("$.author", is("Bruce Eckel")));
	}

	@Test
	public void bookSearch() throws Exception {
		bookRepository.save(new Book("Head First Java, 2nd Edition", "Kathy Sierra, Bert Bates",
				"Those looking to learn coding in JAVA, should refer to Head First Java, 2nd Edition.", getDate(),
				getDate()));
		
		mockMvc.perform(get("/api/{userId}/book/find?keyword={keyword}", user.getId(), "").contentType(BookUtilTest.contentType)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(3)));

		mockMvc.perform(get("/api/{userId}/book/find?keyword={keyword}", user.getId(), "Thinking In Java").contentType(BookUtilTest.contentType)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)));
	}

}
